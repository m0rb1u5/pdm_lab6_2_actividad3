package com.m0rb1u5.pdm_lab6_3_actividad3;

/**
 * Created by m0rb1u5 on 21/02/16.
 */
public interface OnLoginListener {
   void onLogin(String usuario, String password);
}
