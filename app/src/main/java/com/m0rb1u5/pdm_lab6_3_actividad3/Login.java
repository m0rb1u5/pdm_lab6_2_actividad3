package com.m0rb1u5.pdm_lab6_3_actividad3;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by m0rb1u5 on 21/02/16.
 */
public class Login extends LinearLayout {
   private OnLoginListener listener;
   private EditText txtUsuario;
   private EditText txtPassword;
   private Button btnLogin;
   private TextView lblMensaje;

   public Login(Context context) {
      super(context);
      inicializar();
   }

   public Login(Context context, AttributeSet attrs) {
      super(context, attrs);
      inicializar();

      TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.Login);
      String textoBoton = a.getString(R.styleable.Login_login_text);
      btnLogin.setText(textoBoton);
      a.recycle();
   }

   private void inicializar() {
      String infService = Context.LAYOUT_INFLATER_SERVICE;
      LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
      li.inflate(R.layout.login, this, true);

      txtUsuario = (EditText) findViewById(R.id.TxtUsuario);
      txtPassword = (EditText)findViewById(R.id.TxtPassword);
      btnLogin = (Button) findViewById(R.id.BtnAceptar);
      lblMensaje = (TextView) findViewById(R.id.LblMensaje);

      asignarEventos();
   }

   public void setOnLoginListener(OnLoginListener listener) {
      this.listener = listener;
   }

   private void asignarEventos() {
      btnLogin.setOnClickListener(new OnClickListener() {
         @Override
         public void onClick(View v) {
            listener.onLogin(txtUsuario.getText().toString(),
                  txtPassword.getText().toString());
         }
      });
   }

   public void setMensaje(String msg) {
      lblMensaje.setText(msg);
   }
}
